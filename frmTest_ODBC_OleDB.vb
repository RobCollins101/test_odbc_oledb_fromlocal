Imports System.Collections.Generic

Public Class frmTest_ODBC_OleDB
	Inherits System.Windows.Forms.Form

	'Private maStringBits As New Hashtable, msConnectStringODBC As String = "", msConnectStringOleDB As String = ""
	Private maStringBits As New Dictionary(Of String, String), msConnectStringODBC As String = "", msConnectStringOleDB As String = ""
	Private aconDB As Odbc.OdbcConnection, oCmd As Odbc.OdbcCommand, oReader As Odbc.OdbcDataReader
	Friend WithEvents cmdConnectOleDB As System.Windows.Forms.Button
	Friend WithEvents txtOleDBConnectString As System.Windows.Forms.TextBox
	Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
	Friend WithEvents lblConnectStringOleDB As System.Windows.Forms.Label

#Region " Windows Form Designer generated code "

	Public Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents lblConnectStringODBC As System.Windows.Forms.Label
	Friend WithEvents txtODBCConnectString As System.Windows.Forms.TextBox
	Friend WithEvents lblSQL As System.Windows.Forms.Label
	Friend WithEvents txtSQL As System.Windows.Forms.TextBox
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents cmdConnectODBC As System.Windows.Forms.Button
	Friend WithEvents grpConnection As System.Windows.Forms.GroupBox
	Friend WithEvents cmbServerTypes As System.Windows.Forms.ComboBox
	Friend WithEvents lblServerType As System.Windows.Forms.Label
	Friend WithEvents lblSQLDatabase As System.Windows.Forms.Label
	Friend WithEvents lblSQLUserID As System.Windows.Forms.Label
	Friend WithEvents lblSQLPassword As System.Windows.Forms.Label
	Friend WithEvents lblSQLServer As System.Windows.Forms.Label
	Friend WithEvents pnlSQL As System.Windows.Forms.Panel
	Friend WithEvents txtSQLDatabase As System.Windows.Forms.TextBox
	Friend WithEvents txtSQLUserID As System.Windows.Forms.TextBox
	Friend WithEvents txtSQLPassword As System.Windows.Forms.TextBox
	Friend WithEvents txtSQLServer As System.Windows.Forms.TextBox
	Friend WithEvents pnlIngres As System.Windows.Forms.Panel
	Friend WithEvents lblIngresDatabase As System.Windows.Forms.Label
	Friend WithEvents lblIngresUserID As System.Windows.Forms.Label
	Friend WithEvents lblIngresPassword As System.Windows.Forms.Label
	Friend WithEvents lblIngresServer As System.Windows.Forms.Label
	Friend WithEvents pnlText As System.Windows.Forms.Panel
	Friend WithEvents txtFileExtensions As System.Windows.Forms.TextBox
	Friend WithEvents txtFileFile As System.Windows.Forms.TextBox
	Friend WithEvents lblFileExtensions As System.Windows.Forms.Label
	Friend WithEvents lblFileFile As System.Windows.Forms.Label
	Friend WithEvents pnlAccess As System.Windows.Forms.Panel
	Friend WithEvents txtAccessFile As System.Windows.Forms.TextBox
	Friend WithEvents lblAccessFile As System.Windows.Forms.Label
	Friend WithEvents txtAccessUID As System.Windows.Forms.TextBox
	Friend WithEvents txtAccessPWD As System.Windows.Forms.TextBox
	Friend WithEvents lblAccessUID As System.Windows.Forms.Label
	Friend WithEvents lblAccessPwd As System.Windows.Forms.Label
	Friend WithEvents txtExlDefaultDir As System.Windows.Forms.TextBox
	Friend WithEvents lblExlDefault As System.Windows.Forms.Label
	Friend WithEvents lblXclFile As System.Windows.Forms.Label
	Friend WithEvents txtIngresDatabase As System.Windows.Forms.TextBox
	Friend WithEvents txtIngresUID As System.Windows.Forms.TextBox
	Friend WithEvents txtIngresPWD As System.Windows.Forms.TextBox
	Friend WithEvents txtIngresServer As System.Windows.Forms.TextBox
	Friend WithEvents chkSQLSecurity As System.Windows.Forms.CheckBox
	Friend WithEvents pnlExl As System.Windows.Forms.Panel
	Friend WithEvents txtExlFile As System.Windows.Forms.TextBox
	Friend WithEvents txtResults As System.Windows.Forms.TextBox
	Friend WithEvents txtExampleSQL As System.Windows.Forms.TextBox
	Friend WithEvents lblExample As System.Windows.Forms.Label
	Friend WithEvents chkExlColHeaders As System.Windows.Forms.CheckBox
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents cmbExlExtendedProps As System.Windows.Forms.ComboBox
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTest_ODBC_OleDB))
		Me.lblConnectStringODBC = New System.Windows.Forms.Label()
		Me.txtODBCConnectString = New System.Windows.Forms.TextBox()
		Me.lblSQL = New System.Windows.Forms.Label()
		Me.txtSQL = New System.Windows.Forms.TextBox()
		Me.txtResults = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.cmdConnectODBC = New System.Windows.Forms.Button()
		Me.grpConnection = New System.Windows.Forms.GroupBox()
		Me.cmdConnectOleDB = New System.Windows.Forms.Button()
		Me.txtOleDBConnectString = New System.Windows.Forms.TextBox()
		Me.lblConnectStringOleDB = New System.Windows.Forms.Label()
		Me.cmbServerTypes = New System.Windows.Forms.ComboBox()
		Me.lblServerType = New System.Windows.Forms.Label()
		Me.pnlSQL = New System.Windows.Forms.Panel()
		Me.chkSQLSecurity = New System.Windows.Forms.CheckBox()
		Me.txtSQLDatabase = New System.Windows.Forms.TextBox()
		Me.txtSQLUserID = New System.Windows.Forms.TextBox()
		Me.txtSQLPassword = New System.Windows.Forms.TextBox()
		Me.txtSQLServer = New System.Windows.Forms.TextBox()
		Me.lblSQLDatabase = New System.Windows.Forms.Label()
		Me.lblSQLUserID = New System.Windows.Forms.Label()
		Me.lblSQLPassword = New System.Windows.Forms.Label()
		Me.lblSQLServer = New System.Windows.Forms.Label()
		Me.pnlIngres = New System.Windows.Forms.Panel()
		Me.txtIngresDatabase = New System.Windows.Forms.TextBox()
		Me.txtIngresUID = New System.Windows.Forms.TextBox()
		Me.txtIngresPWD = New System.Windows.Forms.TextBox()
		Me.txtIngresServer = New System.Windows.Forms.TextBox()
		Me.lblIngresDatabase = New System.Windows.Forms.Label()
		Me.lblIngresUserID = New System.Windows.Forms.Label()
		Me.lblIngresPassword = New System.Windows.Forms.Label()
		Me.lblIngresServer = New System.Windows.Forms.Label()
		Me.pnlText = New System.Windows.Forms.Panel()
		Me.txtFileExtensions = New System.Windows.Forms.TextBox()
		Me.txtFileFile = New System.Windows.Forms.TextBox()
		Me.lblFileExtensions = New System.Windows.Forms.Label()
		Me.lblFileFile = New System.Windows.Forms.Label()
		Me.pnlAccess = New System.Windows.Forms.Panel()
		Me.txtAccessUID = New System.Windows.Forms.TextBox()
		Me.txtAccessPWD = New System.Windows.Forms.TextBox()
		Me.lblAccessUID = New System.Windows.Forms.Label()
		Me.lblAccessPwd = New System.Windows.Forms.Label()
		Me.txtAccessFile = New System.Windows.Forms.TextBox()
		Me.lblAccessFile = New System.Windows.Forms.Label()
		Me.pnlExl = New System.Windows.Forms.Panel()
		Me.cmbExlExtendedProps = New System.Windows.Forms.ComboBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.chkExlColHeaders = New System.Windows.Forms.CheckBox()
		Me.txtExlDefaultDir = New System.Windows.Forms.TextBox()
		Me.txtExlFile = New System.Windows.Forms.TextBox()
		Me.lblExlDefault = New System.Windows.Forms.Label()
		Me.lblXclFile = New System.Windows.Forms.Label()
		Me.txtExampleSQL = New System.Windows.Forms.TextBox()
		Me.lblExample = New System.Windows.Forms.Label()
		Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
		Me.grpConnection.SuspendLayout()
		Me.pnlSQL.SuspendLayout()
		Me.pnlIngres.SuspendLayout()
		Me.pnlText.SuspendLayout()
		Me.pnlAccess.SuspendLayout()
		Me.pnlExl.SuspendLayout()
		Me.SuspendLayout()
		'
		'lblConnectStringODBC
		'
		Me.lblConnectStringODBC.AutoSize = True
		Me.lblConnectStringODBC.Location = New System.Drawing.Point(4, 8)
		Me.lblConnectStringODBC.Name = "lblConnectStringODBC"
		Me.lblConnectStringODBC.Size = New System.Drawing.Size(110, 13)
		Me.lblConnectStringODBC.TabIndex = 2
		Me.lblConnectStringODBC.Text = "ODBC Connect String"
		'
		'txtODBCConnectString
		'
		Me.txtODBCConnectString.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
					 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtODBCConnectString.Location = New System.Drawing.Point(8, 24)
		Me.txtODBCConnectString.Name = "txtODBCConnectString"
		Me.txtODBCConnectString.Size = New System.Drawing.Size(737, 20)
		Me.txtODBCConnectString.TabIndex = 99
		'
		'lblSQL
		'
		Me.lblSQL.AutoSize = True
		Me.lblSQL.Location = New System.Drawing.Point(10, 98)
		Me.lblSQL.Name = "lblSQL"
		Me.lblSQL.Size = New System.Drawing.Size(28, 13)
		Me.lblSQL.TabIndex = 4
		Me.lblSQL.Text = "SQL"
		'
		'txtSQL
		'
		Me.txtSQL.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
					 Or System.Windows.Forms.AnchorStyles.Left) _
					 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtSQL.HideSelection = False
		Me.txtSQL.Location = New System.Drawing.Point(10, 114)
		Me.txtSQL.Multiline = True
		Me.txtSQL.Name = "txtSQL"
		Me.txtSQL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtSQL.Size = New System.Drawing.Size(733, 45)
		Me.txtSQL.TabIndex = 100
		Me.txtSQL.Text = "Select count(*) from SysObjects"
		'
		'txtResults
		'
		Me.txtResults.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
					 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtResults.BackColor = System.Drawing.SystemColors.Control
		Me.txtResults.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResults.Location = New System.Drawing.Point(8, 204)
		Me.txtResults.Multiline = True
		Me.txtResults.Name = "txtResults"
		Me.txtResults.ReadOnly = True
		Me.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both
		Me.txtResults.Size = New System.Drawing.Size(733, 185)
		Me.txtResults.TabIndex = 6
		Me.txtResults.WordWrap = False
		'
		'Label1
		'
		Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(14, 167)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(42, 13)
		Me.Label1.TabIndex = 7
		Me.Label1.Text = "Results"
		'
		'cmdConnectODBC
		'
		Me.cmdConnectODBC.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.cmdConnectODBC.Location = New System.Drawing.Point(106, 146)
		Me.cmdConnectODBC.Name = "cmdConnectODBC"
		Me.cmdConnectODBC.Size = New System.Drawing.Size(116, 39)
		Me.cmdConnectODBC.TabIndex = 101
		Me.cmdConnectODBC.Text = "Attempt ODBC connection"
		'
		'grpConnection
		'
		Me.grpConnection.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
					 Or System.Windows.Forms.AnchorStyles.Left) _
					 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.grpConnection.Controls.Add(Me.cmdConnectOleDB)
		Me.grpConnection.Controls.Add(Me.txtOleDBConnectString)
		Me.grpConnection.Controls.Add(Me.lblConnectStringOleDB)
		Me.grpConnection.Controls.Add(Me.cmdConnectODBC)
		Me.grpConnection.Controls.Add(Me.Label1)
		Me.grpConnection.Controls.Add(Me.txtResults)
		Me.grpConnection.Controls.Add(Me.txtSQL)
		Me.grpConnection.Controls.Add(Me.lblSQL)
		Me.grpConnection.Controls.Add(Me.txtODBCConnectString)
		Me.grpConnection.Controls.Add(Me.lblConnectStringODBC)
		Me.grpConnection.Location = New System.Drawing.Point(12, 195)
		Me.grpConnection.Name = "grpConnection"
		Me.grpConnection.Size = New System.Drawing.Size(749, 397)
		Me.grpConnection.TabIndex = 10
		Me.grpConnection.TabStop = False
		'
		'cmdConnectOleDB
		'
		Me.cmdConnectOleDB.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.cmdConnectOleDB.Location = New System.Drawing.Point(269, 146)
		Me.cmdConnectOleDB.Name = "cmdConnectOleDB"
		Me.cmdConnectOleDB.Size = New System.Drawing.Size(116, 39)
		Me.cmdConnectOleDB.TabIndex = 104
		Me.cmdConnectOleDB.Text = "Attempt OleDB connection"
		'
		'txtOleDBConnectString
		'
		Me.txtOleDBConnectString.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
					 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtOleDBConnectString.Location = New System.Drawing.Point(10, 63)
		Me.txtOleDBConnectString.Name = "txtOleDBConnectString"
		Me.txtOleDBConnectString.ReadOnly = True
		Me.txtOleDBConnectString.Size = New System.Drawing.Size(737, 20)
		Me.txtOleDBConnectString.TabIndex = 103
		'
		'lblConnectStringOleDB
		'
		Me.lblConnectStringOleDB.AutoSize = True
		Me.lblConnectStringOleDB.Location = New System.Drawing.Point(6, 47)
		Me.lblConnectStringOleDB.Name = "lblConnectStringOleDB"
		Me.lblConnectStringOleDB.Size = New System.Drawing.Size(111, 13)
		Me.lblConnectStringOleDB.TabIndex = 102
		Me.lblConnectStringOleDB.Text = "OleDB Connect String"
		'
		'cmbServerTypes
		'
		Me.cmbServerTypes.ItemHeight = 13
		Me.cmbServerTypes.Items.AddRange(New Object() {"Access", "CA-OpenIngres", "Excel", "MS SQL Server", "Text File"})
		Me.cmbServerTypes.Location = New System.Drawing.Point(136, 12)
		Me.cmbServerTypes.Name = "cmbServerTypes"
		Me.cmbServerTypes.Size = New System.Drawing.Size(180, 21)
		Me.cmbServerTypes.Sorted = True
		Me.cmbServerTypes.TabIndex = 1
		'
		'lblServerType
		'
		Me.lblServerType.AutoSize = True
		Me.lblServerType.Location = New System.Drawing.Point(24, 12)
		Me.lblServerType.Name = "lblServerType"
		Me.lblServerType.Size = New System.Drawing.Size(62, 13)
		Me.lblServerType.TabIndex = 4
		Me.lblServerType.Text = "ServerType"
		'
		'pnlSQL
		'
		Me.pnlSQL.Controls.Add(Me.chkSQLSecurity)
		Me.pnlSQL.Controls.Add(Me.txtSQLDatabase)
		Me.pnlSQL.Controls.Add(Me.txtSQLUserID)
		Me.pnlSQL.Controls.Add(Me.txtSQLPassword)
		Me.pnlSQL.Controls.Add(Me.txtSQLServer)
		Me.pnlSQL.Controls.Add(Me.lblSQLDatabase)
		Me.pnlSQL.Controls.Add(Me.lblSQLUserID)
		Me.pnlSQL.Controls.Add(Me.lblSQLPassword)
		Me.pnlSQL.Controls.Add(Me.lblSQLServer)
		Me.pnlSQL.Location = New System.Drawing.Point(16, 44)
		Me.pnlSQL.Name = "pnlSQL"
		Me.pnlSQL.Size = New System.Drawing.Size(344, 142)
		Me.pnlSQL.TabIndex = 11
		Me.pnlSQL.Visible = False
		'
		'chkSQLSecurity
		'
		Me.chkSQLSecurity.Location = New System.Drawing.Point(28, 60)
		Me.chkSQLSecurity.Name = "chkSQLSecurity"
		Me.chkSQLSecurity.Size = New System.Drawing.Size(152, 16)
		Me.chkSQLSecurity.TabIndex = 9
		Me.chkSQLSecurity.Text = "Use Windows Security"
		'
		'txtSQLDatabase
		'
		Me.txtSQLDatabase.Location = New System.Drawing.Point(80, 28)
		Me.txtSQLDatabase.Name = "txtSQLDatabase"
		Me.txtSQLDatabase.Size = New System.Drawing.Size(248, 20)
		Me.txtSQLDatabase.TabIndex = 8
		'
		'txtSQLUserID
		'
		Me.txtSQLUserID.Location = New System.Drawing.Point(76, 84)
		Me.txtSQLUserID.Name = "txtSQLUserID"
		Me.txtSQLUserID.Size = New System.Drawing.Size(248, 20)
		Me.txtSQLUserID.TabIndex = 10
		'
		'txtSQLPassword
		'
		Me.txtSQLPassword.Location = New System.Drawing.Point(76, 108)
		Me.txtSQLPassword.Name = "txtSQLPassword"
		Me.txtSQLPassword.Size = New System.Drawing.Size(248, 20)
		Me.txtSQLPassword.TabIndex = 11
		'
		'txtSQLServer
		'
		Me.txtSQLServer.Location = New System.Drawing.Point(80, 4)
		Me.txtSQLServer.Name = "txtSQLServer"
		Me.txtSQLServer.Size = New System.Drawing.Size(248, 20)
		Me.txtSQLServer.TabIndex = 7
		'
		'lblSQLDatabase
		'
		Me.lblSQLDatabase.AutoSize = True
		Me.lblSQLDatabase.Location = New System.Drawing.Point(24, 32)
		Me.lblSQLDatabase.Name = "lblSQLDatabase"
		Me.lblSQLDatabase.Size = New System.Drawing.Size(53, 13)
		Me.lblSQLDatabase.TabIndex = 13
		Me.lblSQLDatabase.Text = "Database"
		'
		'lblSQLUserID
		'
		Me.lblSQLUserID.AutoSize = True
		Me.lblSQLUserID.Location = New System.Drawing.Point(20, 92)
		Me.lblSQLUserID.Name = "lblSQLUserID"
		Me.lblSQLUserID.Size = New System.Drawing.Size(40, 13)
		Me.lblSQLUserID.TabIndex = 12
		Me.lblSQLUserID.Text = "UserID"
		'
		'lblSQLPassword
		'
		Me.lblSQLPassword.AutoSize = True
		Me.lblSQLPassword.Location = New System.Drawing.Point(12, 112)
		Me.lblSQLPassword.Name = "lblSQLPassword"
		Me.lblSQLPassword.Size = New System.Drawing.Size(53, 13)
		Me.lblSQLPassword.TabIndex = 11
		Me.lblSQLPassword.Text = "Password"
		'
		'lblSQLServer
		'
		Me.lblSQLServer.AutoSize = True
		Me.lblSQLServer.Location = New System.Drawing.Point(24, 8)
		Me.lblSQLServer.Name = "lblSQLServer"
		Me.lblSQLServer.Size = New System.Drawing.Size(38, 13)
		Me.lblSQLServer.TabIndex = 10
		Me.lblSQLServer.Text = "Server"
		'
		'pnlIngres
		'
		Me.pnlIngres.Controls.Add(Me.txtIngresDatabase)
		Me.pnlIngres.Controls.Add(Me.txtIngresUID)
		Me.pnlIngres.Controls.Add(Me.txtIngresPWD)
		Me.pnlIngres.Controls.Add(Me.txtIngresServer)
		Me.pnlIngres.Controls.Add(Me.lblIngresDatabase)
		Me.pnlIngres.Controls.Add(Me.lblIngresUserID)
		Me.pnlIngres.Controls.Add(Me.lblIngresPassword)
		Me.pnlIngres.Controls.Add(Me.lblIngresServer)
		Me.pnlIngres.Location = New System.Drawing.Point(16, 44)
		Me.pnlIngres.Name = "pnlIngres"
		Me.pnlIngres.Size = New System.Drawing.Size(344, 142)
		Me.pnlIngres.TabIndex = 12
		Me.pnlIngres.Visible = False
		'
		'txtIngresDatabase
		'
		Me.txtIngresDatabase.Location = New System.Drawing.Point(80, 28)
		Me.txtIngresDatabase.Name = "txtIngresDatabase"
		Me.txtIngresDatabase.Size = New System.Drawing.Size(244, 20)
		Me.txtIngresDatabase.TabIndex = 15
		'
		'txtIngresUID
		'
		Me.txtIngresUID.Location = New System.Drawing.Point(80, 56)
		Me.txtIngresUID.Name = "txtIngresUID"
		Me.txtIngresUID.Size = New System.Drawing.Size(244, 20)
		Me.txtIngresUID.TabIndex = 16
		'
		'txtIngresPWD
		'
		Me.txtIngresPWD.Location = New System.Drawing.Point(80, 80)
		Me.txtIngresPWD.Name = "txtIngresPWD"
		Me.txtIngresPWD.Size = New System.Drawing.Size(244, 20)
		Me.txtIngresPWD.TabIndex = 17
		'
		'txtIngresServer
		'
		Me.txtIngresServer.Location = New System.Drawing.Point(80, 4)
		Me.txtIngresServer.Name = "txtIngresServer"
		Me.txtIngresServer.Size = New System.Drawing.Size(244, 20)
		Me.txtIngresServer.TabIndex = 14
		'
		'lblIngresDatabase
		'
		Me.lblIngresDatabase.AutoSize = True
		Me.lblIngresDatabase.Location = New System.Drawing.Point(16, 32)
		Me.lblIngresDatabase.Name = "lblIngresDatabase"
		Me.lblIngresDatabase.Size = New System.Drawing.Size(53, 13)
		Me.lblIngresDatabase.TabIndex = 13
		Me.lblIngresDatabase.Text = "Database"
		'
		'lblIngresUserID
		'
		Me.lblIngresUserID.AutoSize = True
		Me.lblIngresUserID.Location = New System.Drawing.Point(28, 60)
		Me.lblIngresUserID.Name = "lblIngresUserID"
		Me.lblIngresUserID.Size = New System.Drawing.Size(40, 13)
		Me.lblIngresUserID.TabIndex = 12
		Me.lblIngresUserID.Text = "UserID"
		'
		'lblIngresPassword
		'
		Me.lblIngresPassword.AutoSize = True
		Me.lblIngresPassword.Location = New System.Drawing.Point(12, 80)
		Me.lblIngresPassword.Name = "lblIngresPassword"
		Me.lblIngresPassword.Size = New System.Drawing.Size(53, 13)
		Me.lblIngresPassword.TabIndex = 11
		Me.lblIngresPassword.Text = "Password"
		'
		'lblIngresServer
		'
		Me.lblIngresServer.AutoSize = True
		Me.lblIngresServer.Location = New System.Drawing.Point(28, 8)
		Me.lblIngresServer.Name = "lblIngresServer"
		Me.lblIngresServer.Size = New System.Drawing.Size(38, 13)
		Me.lblIngresServer.TabIndex = 10
		Me.lblIngresServer.Text = "Server"
		'
		'pnlText
		'
		Me.pnlText.Controls.Add(Me.txtFileExtensions)
		Me.pnlText.Controls.Add(Me.txtFileFile)
		Me.pnlText.Controls.Add(Me.lblFileExtensions)
		Me.pnlText.Controls.Add(Me.lblFileFile)
		Me.pnlText.Location = New System.Drawing.Point(16, 44)
		Me.pnlText.Name = "pnlText"
		Me.pnlText.Size = New System.Drawing.Size(344, 142)
		Me.pnlText.TabIndex = 13
		Me.pnlText.Visible = False
		'
		'txtFileExtensions
		'
		Me.txtFileExtensions.Location = New System.Drawing.Point(80, 28)
		Me.txtFileExtensions.Name = "txtFileExtensions"
		Me.txtFileExtensions.Size = New System.Drawing.Size(248, 20)
		Me.txtFileExtensions.TabIndex = 13
		'
		'txtFileFile
		'
		Me.txtFileFile.Location = New System.Drawing.Point(80, 4)
		Me.txtFileFile.Name = "txtFileFile"
		Me.txtFileFile.Size = New System.Drawing.Size(248, 20)
		Me.txtFileFile.TabIndex = 12
		'
		'lblFileExtensions
		'
		Me.lblFileExtensions.AutoSize = True
		Me.lblFileExtensions.Location = New System.Drawing.Point(24, 32)
		Me.lblFileExtensions.Name = "lblFileExtensions"
		Me.lblFileExtensions.Size = New System.Drawing.Size(58, 13)
		Me.lblFileExtensions.TabIndex = 13
		Me.lblFileExtensions.Text = "Extensions"
		'
		'lblFileFile
		'
		Me.lblFileFile.AutoSize = True
		Me.lblFileFile.Location = New System.Drawing.Point(24, 8)
		Me.lblFileFile.Name = "lblFileFile"
		Me.lblFileFile.Size = New System.Drawing.Size(23, 13)
		Me.lblFileFile.TabIndex = 10
		Me.lblFileFile.Text = "File"
		'
		'pnlAccess
		'
		Me.pnlAccess.Controls.Add(Me.txtAccessUID)
		Me.pnlAccess.Controls.Add(Me.txtAccessPWD)
		Me.pnlAccess.Controls.Add(Me.lblAccessUID)
		Me.pnlAccess.Controls.Add(Me.lblAccessPwd)
		Me.pnlAccess.Controls.Add(Me.txtAccessFile)
		Me.pnlAccess.Controls.Add(Me.lblAccessFile)
		Me.pnlAccess.Location = New System.Drawing.Point(16, 44)
		Me.pnlAccess.Name = "pnlAccess"
		Me.pnlAccess.Size = New System.Drawing.Size(344, 145)
		Me.pnlAccess.TabIndex = 14
		Me.pnlAccess.Visible = False
		'
		'txtAccessUID
		'
		Me.txtAccessUID.Location = New System.Drawing.Point(80, 32)
		Me.txtAccessUID.Name = "txtAccessUID"
		Me.txtAccessUID.Size = New System.Drawing.Size(248, 20)
		Me.txtAccessUID.TabIndex = 5
		Me.txtAccessUID.Text = "Admin"
		'
		'txtAccessPWD
		'
		Me.txtAccessPWD.Location = New System.Drawing.Point(80, 56)
		Me.txtAccessPWD.Name = "txtAccessPWD"
		Me.txtAccessPWD.Size = New System.Drawing.Size(248, 20)
		Me.txtAccessPWD.TabIndex = 6
		'
		'lblAccessUID
		'
		Me.lblAccessUID.AutoSize = True
		Me.lblAccessUID.Location = New System.Drawing.Point(24, 36)
		Me.lblAccessUID.Name = "lblAccessUID"
		Me.lblAccessUID.Size = New System.Drawing.Size(40, 13)
		Me.lblAccessUID.TabIndex = 19
		Me.lblAccessUID.Text = "UserID"
		'
		'lblAccessPwd
		'
		Me.lblAccessPwd.AutoSize = True
		Me.lblAccessPwd.Location = New System.Drawing.Point(16, 60)
		Me.lblAccessPwd.Name = "lblAccessPwd"
		Me.lblAccessPwd.Size = New System.Drawing.Size(53, 13)
		Me.lblAccessPwd.TabIndex = 18
		Me.lblAccessPwd.Text = "Password"
		'
		'txtAccessFile
		'
		Me.txtAccessFile.Location = New System.Drawing.Point(80, 4)
		Me.txtAccessFile.Name = "txtAccessFile"
		Me.txtAccessFile.Size = New System.Drawing.Size(248, 20)
		Me.txtAccessFile.TabIndex = 4
		'
		'lblAccessFile
		'
		Me.lblAccessFile.AutoSize = True
		Me.lblAccessFile.Location = New System.Drawing.Point(24, 8)
		Me.lblAccessFile.Name = "lblAccessFile"
		Me.lblAccessFile.Size = New System.Drawing.Size(23, 13)
		Me.lblAccessFile.TabIndex = 10
		Me.lblAccessFile.Text = "File"
		'
		'pnlExl
		'
		Me.pnlExl.Controls.Add(Me.cmbExlExtendedProps)
		Me.pnlExl.Controls.Add(Me.Label2)
		Me.pnlExl.Controls.Add(Me.chkExlColHeaders)
		Me.pnlExl.Controls.Add(Me.txtExlDefaultDir)
		Me.pnlExl.Controls.Add(Me.txtExlFile)
		Me.pnlExl.Controls.Add(Me.lblExlDefault)
		Me.pnlExl.Controls.Add(Me.lblXclFile)
		Me.pnlExl.Location = New System.Drawing.Point(16, 44)
		Me.pnlExl.Name = "pnlExl"
		Me.pnlExl.Size = New System.Drawing.Size(344, 145)
		Me.pnlExl.TabIndex = 15
		Me.pnlExl.Visible = False
		'
		'cmbExlExtendedProps
		'
		Me.cmbExlExtendedProps.Items.AddRange(New Object() {"Excel 8.0", "Excel 5.0"})
		Me.cmbExlExtendedProps.Location = New System.Drawing.Point(100, 88)
		Me.cmbExlExtendedProps.Name = "cmbExlExtendedProps"
		Me.cmbExlExtendedProps.Size = New System.Drawing.Size(204, 21)
		Me.cmbExlExtendedProps.TabIndex = 16
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(16, 84)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(64, 28)
		Me.Label2.TabIndex = 15
		Me.Label2.Text = "Extended Properties"
		'
		'chkExlColHeaders
		'
		Me.chkExlColHeaders.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.chkExlColHeaders.Location = New System.Drawing.Point(16, 56)
		Me.chkExlColHeaders.Name = "chkExlColHeaders"
		Me.chkExlColHeaders.Size = New System.Drawing.Size(80, 20)
		Me.chkExlColHeaders.TabIndex = 14
		Me.chkExlColHeaders.Text = "Col Hdrs"
		'
		'txtExlDefaultDir
		'
		Me.txtExlDefaultDir.Location = New System.Drawing.Point(80, 28)
		Me.txtExlDefaultDir.Name = "txtExlDefaultDir"
		Me.txtExlDefaultDir.Size = New System.Drawing.Size(248, 20)
		Me.txtExlDefaultDir.TabIndex = 3
		'
		'txtExlFile
		'
		Me.txtExlFile.Location = New System.Drawing.Point(80, 4)
		Me.txtExlFile.Name = "txtExlFile"
		Me.txtExlFile.Size = New System.Drawing.Size(248, 20)
		Me.txtExlFile.TabIndex = 2
		'
		'lblExlDefault
		'
		Me.lblExlDefault.AutoSize = True
		Me.lblExlDefault.Location = New System.Drawing.Point(16, 32)
		Me.lblExlDefault.Name = "lblExlDefault"
		Me.lblExlDefault.Size = New System.Drawing.Size(54, 13)
		Me.lblExlDefault.TabIndex = 13
		Me.lblExlDefault.Text = "DefaultDir"
		'
		'lblXclFile
		'
		Me.lblXclFile.AutoSize = True
		Me.lblXclFile.Location = New System.Drawing.Point(24, 8)
		Me.lblXclFile.Name = "lblXclFile"
		Me.lblXclFile.Size = New System.Drawing.Size(23, 13)
		Me.lblXclFile.TabIndex = 10
		Me.lblXclFile.Text = "File"
		'
		'txtExampleSQL
		'
		Me.txtExampleSQL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
					 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtExampleSQL.BackColor = System.Drawing.SystemColors.Control
		Me.txtExampleSQL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtExampleSQL.Location = New System.Drawing.Point(380, 48)
		Me.txtExampleSQL.Multiline = True
		Me.txtExampleSQL.Name = "txtExampleSQL"
		Me.txtExampleSQL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtExampleSQL.Size = New System.Drawing.Size(373, 138)
		Me.txtExampleSQL.TabIndex = 16
		'
		'lblExample
		'
		Me.lblExample.AutoSize = True
		Me.lblExample.Location = New System.Drawing.Point(380, 28)
		Me.lblExample.Name = "lblExample"
		Me.lblExample.Size = New System.Drawing.Size(80, 13)
		Me.lblExample.TabIndex = 17
		Me.lblExample.Text = "Example Select"
		'
		'LinkLabel1
		'
		Me.LinkLabel1.AutoSize = True
		Me.LinkLabel1.Location = New System.Drawing.Point(478, 29)
		Me.LinkLabel1.Name = "LinkLabel1"
		Me.LinkLabel1.Size = New System.Drawing.Size(140, 13)
		Me.LinkLabel1.TabIndex = 18
		Me.LinkLabel1.TabStop = True
		Me.LinkLabel1.Text = "www.connectionstrings.com"
		'
		'frmTest_ODBC_OleDB
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(765, 602)
		Me.Controls.Add(Me.LinkLabel1)
		Me.Controls.Add(Me.lblExample)
		Me.Controls.Add(Me.txtExampleSQL)
		Me.Controls.Add(Me.grpConnection)
		Me.Controls.Add(Me.cmbServerTypes)
		Me.Controls.Add(Me.lblServerType)
		Me.Controls.Add(Me.pnlIngres)
		Me.Controls.Add(Me.pnlSQL)
		Me.Controls.Add(Me.pnlText)
		Me.Controls.Add(Me.pnlAccess)
		Me.Controls.Add(Me.pnlExl)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MinimumSize = New System.Drawing.Size(658, 630)
		Me.Name = "frmTest_ODBC_OleDB"
		Me.Text = "Test ODBC and OleDB connectivity"
		Me.grpConnection.ResumeLayout(False)
		Me.grpConnection.PerformLayout()
		Me.pnlSQL.ResumeLayout(False)
		Me.pnlSQL.PerformLayout()
		Me.pnlIngres.ResumeLayout(False)
		Me.pnlIngres.PerformLayout()
		Me.pnlText.ResumeLayout(False)
		Me.pnlText.PerformLayout()
		Me.pnlAccess.ResumeLayout(False)
		Me.pnlAccess.PerformLayout()
		Me.pnlExl.ResumeLayout(False)
		Me.pnlExl.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

	Private Sub cmbServerTypes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbServerTypes.SelectedIndexChanged
		GetMyPanel()
	End Sub

	Private Sub cmbServerTypes_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbServerTypes.LostFocus

	End Sub

	Private Sub GetMyPanel()
		pnlIngres.Visible = False
		pnlIngres.Enabled = False
		pnlSQL.Visible = False
		pnlSQL.Enabled = False
		pnlExl.Enabled = False
		pnlExl.Visible = False
		pnlAccess.Enabled = False
		pnlAccess.Visible = False
		pnlText.Enabled = False
		pnlText.Visible = False
		txtExampleSQL.Text = ""

		Select Case cmbServerTypes.Text
			Case "CA-OpenIngres"
				pnlIngres.Visible = True
				pnlIngres.Enabled = True
				txtExampleSQL.Text = "SELECT Table.ColSpec from Table" & vbCrLf & _
				"Table.ColSpec is an ODBC syntax and Table is a table spec within the Ingres database" & vbCrLf & _
				"You can use any recognised ODBC SQL command here"
				txtSQL.Text = "SELECT (dbmsinfo('username'))"
			Case "MS SQL Server"
				pnlSQL.Visible = True
				pnlSQL.Enabled = True
				txtExampleSQL.Text = "SELECT Table.ColSpec from Table" & vbCrLf & _
				 "Table.ColSpec is an ODBC syntax and Table is a table spec within the MS SQL database" & vbCrLf & _
				"You can use any standard recognised ODBC SQL command here"
				txtSQL.Text = "Select getdate()"
			Case "Text File"
				pnlText.Visible = True
				pnlText.Enabled = True
				txtExampleSQL.Text = "select * from [sample2#txt]" & vbCrLf & _
				"This requires a Schema.ini file in the same path as the delimited text file." & vbCrLf & _
				"[SAMPLE2.TXT]" & vbCrLf & _
				"ColNameHeader = False" & vbCrLf & _
				"Format = CSVDelimited" & vbCrLf & _
				"CharacterSet = ANSI" & vbCrLf & _
				"Col1=ProductID short" & vbCrLf & _
				"Col2=ProductName char width 30" & vbCrLf & _
				"Col3=Discontinued bit" & vbCrLf & vbCrLf & _
				"Example comma delimited file Sample2.txt." & vbCrLf & _
				"1,Chai,true" & vbCrLf & _
				"2,Chang,false"
			Case "Excel"
				pnlExl.Visible = True
				pnlExl.Enabled = True
				txtExampleSQL.Text = "SELECT ColSpec FROM [sheet1$]" & vbCrLf & _
				" or " & vbCrLf & _
				"SELECT * FROM `Sheet1$`" & vbCrLf & _
				" for all of a sheet or " & vbCrLf & _
				"SELECT * FROM [Sheet1$A1:B10]" & vbCrLf & _
				"for an unnamed range or " & vbCrLf & _
				"SELECT * FROM MyRange" & vbCrLf & _
				 "where MyRange is a specified range in the worksheet"
				txtSQL.Text = "SELECT * FROM `Sheet1$`"
			Case "Access"
				pnlAccess.Visible = True
				pnlAccess.Enabled = True
				txtExampleSQL.Text = "SELECT Table.ColSpec from Table" & vbCrLf & _
				 "Table.ColSpec is an ODBC syntax and Table is a table spec within the Access database" & vbCrLf & _
				"You can use any standard recognised ODBC SQL command here"
				txtSQL.Text = "Select NOW"
			Case Else
				pnlSQL.Visible = True
				pnlSQL.Enabled = True
				txtExampleSQL.Text = "SELECT Table.ColSpec from Table" & vbCrLf & _
				 "Table.ColSpec is an ODBC syntax and Table is a table spec within the SQL database" & vbCrLf & _
				"You can use any standard recognised ODBC SQL command here"
				txtSQL.Text = "Select GetDate()"
		End Select
	End Sub

	Private Sub ConnectToList()
		Dim aBits() As String = Split(msConnectStringODBC, ";")
		Dim aKey() As String, iPos As Long, iMax As Long = UBound(aBits)
		Dim sKey As String = "", sValue As String = ""
		Dim sServer As String = ""
		Dim sDatabase As String = ""
		Dim sUserID As String = ""
		Dim sPassword As String = ""
		Dim sSecurity As String = ""
		Dim bSecurity As Boolean = False
		Dim sFile As String = ""
		Dim sExtensions As String = ""
		Dim sDefaultDir As String = ""
		Dim bColHeaders As Boolean = True
		Dim sExtendedProperties As String = ""

		Static bInActivity As Boolean

		If Not (bInActivity) Then
			bInActivity = True

			' Step 1 : generate the key list
			maStringBits.Clear()
			For iPos = 0 To iMax
				aKey = Split(aBits(iPos), "=")
				If UBound(aKey) = 1 Then
					sKey = UCase(aKey(0))
					sValue = aKey(1)
					maStringBits.Add(sKey, sValue)
				End If
				If sKey = "DRIVER" Then
					Select Case sValue
						Case "CA-OpenIngres", "Ingres" : cmbServerTypes.Text = "CA-OpenIngres"
						Case "{SQL Server}" : cmbServerTypes.Text = "MS SQL Server"
						Case "{Microsoft Text Driver (*.txt; *.csv)}" : cmbServerTypes.Text = "Text File"
						Case "{Microsoft Access Driver (*.mdb)}" : cmbServerTypes.Text = "Access"
						Case "{Microsoft Excel Driver (*.xls)}" : cmbServerTypes.Text = "Excel"
					End Select
					GetMyPanel()
					'cmbServerTypes.Text = sValue
				End If

			Next

			' Step 2 : locate the fields that relate to the visual components
			Select Case cmbServerTypes.Text
				Case "CA-OpenIngres"
					If maStringBits.ContainsKey("SERVER") Then sServer = maStringBits.Item("SERVER")
					If maStringBits.ContainsKey("DATABASE") Then sDatabase = maStringBits.Item("DATABASE")
					If maStringBits.ContainsKey("UID") Then sUserID = maStringBits.Item("UID")
					If maStringBits.ContainsKey("PWD") Then sPassword = maStringBits.Item("PWD")
					txtIngresServer.Text = sServer
					txtIngresDatabase.Text = sDatabase
					txtIngresUID.Text = sUserID
					txtIngresPWD.Text = sPassword
				Case "MS SQL Server"
					If maStringBits.ContainsKey("SERVER") Then sServer = maStringBits.Item("SERVER")
					If maStringBits.ContainsKey("DATABASE") Then sDatabase = maStringBits.Item("DATABASE")
					If maStringBits.ContainsKey("UID") Then sUserID = maStringBits.Item("UID")
					If maStringBits.ContainsKey("PWD") Then sPassword = maStringBits.Item("PWD")
					If maStringBits.ContainsKey("TRUSTED_SECURITY") Then bSecurity = True
					txtSQLServer.Text = sServer
					txtSQLDatabase.Text = sDatabase
					txtSQLUserID.Text = sUserID
					txtSQLPassword.Text = sPassword
					chkSQLSecurity.Checked = bSecurity
				Case "Text File"
					If maStringBits.ContainsKey("DBQ") Then sFile = maStringBits.Item("Dbq")
					If maStringBits.ContainsKey("EXTENSIONS") Then sExtensions = maStringBits.Item("Extensions")
					txtFileFile.Text = sFile
					txtFileExtensions.Text = sExtensions
				Case "Access"
					If maStringBits.ContainsKey("DBQ") Then sFile = maStringBits.Item("Dbq")
					If maStringBits.ContainsKey("UID") Then sUserID = maStringBits.Item("UID")
					If maStringBits.ContainsKey("PWD") Then sPassword = maStringBits.Item("PWD")
					txtAccessFile.Text = sFile
					txtAccessUID.Text = sUserID
					txtAccessPWD.Text = sPassword
				Case "Excel"
					If maStringBits.ContainsKey("DBQ") Then sFile = maStringBits.Item("Dbq")
					If maStringBits.ContainsKey("DEFAULTDIR") Then sDefaultDir = maStringBits.Item("DefaultDir")
					If maStringBits.ContainsKey("HDR") Then bColHeaders = (UCase(maStringBits.Item("HDR")) <> "NO")
					If maStringBits.ContainsKey("EXTENDED PROPERTIES") Then sExtendedProperties = maStringBits.Item("EXTENDED PROPERTIES")
					txtExlFile.Text = sFile
					txtExlDefaultDir.Text = sDefaultDir
					chkExlColHeaders.Checked = bColHeaders
					cmbExlExtendedProps.Text = sExtendedProperties
				Case Else
			End Select

			' Step 3 : Regenerate the string correctly

			GenConnectString()

			bInActivity = False


		End If
	End Sub


	Private Sub ConnectToListOleDB()


	End Sub


	Private Sub GenConnectString()
		Dim sString As String = "", sKey As String, sValue As String

		maStringBits.Clear()
		Select Case cmbServerTypes.Text
			Case "CA-OpenIngres"
				maStringBits.Add("DRIVER", "CA-OpenIngres")
				maStringBits.Add("SERVERTYPE", "INGRES")
				maStringBits.Add("SERVER", txtIngresServer.Text)
				maStringBits.Add("DATABASE", txtIngresDatabase.Text)
				maStringBits.Add("UID", txtIngresUID.Text)
				maStringBits.Add("PWD", txtIngresPWD.Text)
			Case "MS SQL Server"
				maStringBits.Add("DRIVER", "SQL Server")
				maStringBits.Add("SERVER", txtSQLServer.Text)
				maStringBits.Add("DATABASE", txtSQLDatabase.Text)
				maStringBits.Add("UID", txtSQLUserID.Text)
				maStringBits.Add("PWD", txtSQLPassword.Text)
				maStringBits.Add("TRUSTED_SECURITY", IIf(chkSQLSecurity.Checked, "Yes", ""))
			Case "Text File"
				maStringBits.Add("DRIVER", "{Microsoft Text Driver (*.txt; *.csv)}")
				maStringBits.Add("DBQ", txtFileFile.Text)
				maStringBits.Add("EXTENSIONS", txtFileExtensions.Text)
			Case "Access"
				maStringBits.Add("DRIVER", "{Microsoft Access Driver (*.mdb)}")
				maStringBits.Add("DBQ", txtAccessFile.Text)
				maStringBits.Add("UID", txtAccessUID.Text)
				maStringBits.Add("PWD", txtAccessPWD.Text)
			Case "Excel"
				maStringBits.Add("DRIVER", "{Microsoft Excel Driver (*.xls)}")
				maStringBits.Add("DBQ", txtExlFile.Text)
				maStringBits.Add("DEFAULTDIR", txtExlDefaultDir.Text)
				maStringBits.Add("HDR", IIf(chkExlColHeaders.Checked, "Yes", "No"))
				maStringBits.Add("EXTENDED PROPERTIES", cmbExlExtendedProps.Text)
			Case Else

		End Select

		For Each sKey In maStringBits.Keys
			sValue = maStringBits.Item(sKey)
			If sValue > "" Then
				sString = sString & sKey & "=" & sValue & ";"
			End If
		Next

		txtODBCConnectString.Text = sString
		'txtOleDBConnectString.Text = ""Provider=SQLOLEDB;Server=" & txtServer.Text & ";Database=" & txtDatabase.Text & ";uid=" & txtUserID.Text & ";pwd=" & txtPassword.Text & ";""
		msConnectStringODBC = sString

		maStringBits.Clear()
		Select Case cmbServerTypes.Text
			'Case "CA-OpenIngres"
			'	maStringBits.Add("DRIVER", "CA-OpenIngres")
			'	maStringBits.Add("SERVERTYPE", "INGRES")
			'	maStringBits.Add("SERVER", txtIngresServer.Text)
			'	maStringBits.Add("DATABASE", txtIngresDatabase.Text)
			'	maStringBits.Add("UID", txtIngresUID.Text)
			'	maStringBits.Add("PWD", txtIngresPWD.Text)
			Case "MS SQL Server"
				maStringBits.Add("Provider", "SQLNCLI10")
				maStringBits.Add("Data Source", txtSQLServer.Text)
				maStringBits.Add("Initial Catalog", txtSQLDatabase.Text)
				If Not chkSQLSecurity.Checked Then
					maStringBits.Add("user id", txtSQLUserID.Text)
					maStringBits.Add("password", txtSQLPassword.Text)
					'maStringBits.Add("Trusted_Connection", "False")
				Else
					maStringBits.Add("Integrated Security", "SSPI")
				End If
			Case "Text File"
				maStringBits.Add("Provider", "Microsoft.Jet.OLEDB.4.0")
				maStringBits.Add("Data Source", txtFileFile.Text)
				maStringBits.Add("Extended Properties", """text;HDR=Yes;FMT=Delimited""")
				'Case "Access"
				'	maStringBits.Add("DRIVER", "{Microsoft Access Driver (*.mdb)}")
				'	maStringBits.Add("DBQ", txtAccessFile.Text)
				'	maStringBits.Add("UID", txtAccessUID.Text)
				'	maStringBits.Add("PWD", txtAccessPWD.Text)
				'Case "Excel"
				'	maStringBits.Add("DRIVER", "{Microsoft Excel Driver (*.xls)}")
				'	maStringBits.Add("DBQ", txtExlFile.Text)
				'	maStringBits.Add("DEFAULTDIR", txtExlDefaultDir.Text)
				'	maStringBits.Add("HDR", IIf(chkExlColHeaders.Checked, "Yes", "No"))
				'	maStringBits.Add("EXTENDED PROPERTIES", cmbExlExtendedProps.Text)
			Case Else

		End Select

		sString = ""

		For Each sKey In maStringBits.Keys
			sValue = maStringBits.Item(sKey)
			If sValue > "" Then
				sString = sString & sKey & "=" & sValue & ";"
			End If
		Next

		msConnectStringOleDB = sString
		txtOleDBConnectString.Text = sString

		cmdConnectODBC.Visible = (msConnectStringODBC <> "")
		cmdConnectOleDB.Visible = (msConnectStringOleDB <> "")

	End Sub


	Private Sub chkSQLSecurity_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSQLSecurity.CheckStateChanged
		GenConnectString()
	End Sub

	Private Sub cmdConnectODBC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdConnectODBC.Click
		Dim iPos As Long, iMax As Long, iCol As Long
		Dim dtTimeStart As New Date, dtTimeEnd As New Date, lDiffTicks As Long, dtTimeDiff As Date
		Try

			cmdConnectODBC.Enabled = False
			dtTimeStart = Now

			txtResults.Text = ""

			Application.DoEvents()
			GenConnectString()
			aconDB = New Odbc.OdbcConnection(msConnectStringODBC)
			aconDB.Open()
			If aconDB.State = ConnectionState.Open Then
				If txtSQL.Text = "" Then
					txtResults.Text = "Connection OK but no SQL execution to run"
				Else
					oCmd = aconDB.CreateCommand
					oCmd.CommandText = txtSQL.Text
					oCmd.CommandType = CommandType.Text
					oReader = oCmd.ExecuteReader
					If oReader.HasRows Then
						txtResults.Text = ""
						iMax = 1500
						iPos = 0
						Do While iMax > iPos And oReader.Read
							Application.DoEvents()
							For iCol = 0 To oReader.FieldCount - 1
								txtResults.Text = txtResults.Text & oReader.GetValue(iCol).ToString & ", "
							Next
							txtResults.Text = txtResults.Text & vbCrLf
							iPos = iPos + 1
						Loop
						Do While oReader.Read
							iPos = iPos + 1
							Application.DoEvents()
						Loop
						dtTimeEnd = Now
						lDiffTicks = dtTimeEnd.Ticks - dtTimeStart.Ticks
						dtTimeDiff = New Date(lDiffTicks)
						txtResults.Text = "total of " & iPos & " records with Duration of " & _
						IIf(dtTimeDiff.Hour > 0, dtTimeDiff.Hour & " hour" & IIf(dtTimeDiff.Hour > 1, "s ", " "), "") & IIf(dtTimeDiff.Minute > 0, dtTimeDiff.Minute & " minute" & IIf(dtTimeDiff.Minute > 1, "s ", " "), "") & dtTimeDiff.Second & " second" & IIf(dtTimeDiff.Second > 1, "s", "") & vbCrLf & _
						txtResults.Text
						oReader.Close()
					End If
				End If
				aconDB.Close()

			Else
				txtResults.Text = "No connection"
			End If

		Catch ex As Exception
			txtResults.Text = txtResults.Text & vbCrLf & ex.ToString
		End Try

		cmdConnectODBC.Enabled = True

	End Sub

	Private Sub txtAccessFile_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAccessFile.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtAccessPWD_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAccessPWD.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtAccessUID_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAccessUID.LostFocus
		GenConnectString()
	End Sub

	'Private Sub txtConnectString_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtConnectString.Click
	'	GenConnectString()
	'End Sub

	Private Sub txtExlDefaultDir_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtExlDefaultDir.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtExlFile_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtExlFile.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtFileExtensions_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFileExtensions.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtFileFile_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFileFile.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtIngresDatabase_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIngresDatabase.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtIngresPWD_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIngresPWD.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtIngresServer_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIngresServer.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtIngresUID_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIngresUID.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtSQLDatabase_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSQLDatabase.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtSQLPassword_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSQLPassword.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtSQLServer_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSQLServer.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtSQLUserID_Validating(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSQLUserID.LostFocus
		GenConnectString()
	End Sub

	Private Sub txtODBCConnectString_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtODBCConnectString.TextChanged
		'		msConnectString = txtConnectString.Text
		'		ConnectToList()
	End Sub

	Private Sub txtODBCConnectString_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtODBCConnectString.Validating
		msConnectStringODBC = txtODBCConnectString.Text
		ConnectToList()
	End Sub

	Private Sub txtOleDBConnectString_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOleDBConnectString.TextChanged
		'		msConnectString = txtConnectString.Text
		'		ConnectToList()
	End Sub

	Private Sub txtOleDBConnectString_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtOleDBConnectString.Validating
		msConnectStringOleDB = txtOleDBConnectString.Text
		ConnectToList()
	End Sub

	Private Sub chkExlColHeaders_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExlColHeaders.CheckedChanged
		GenConnectString()

	End Sub

	Private Sub txtSQL_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSQL.LostFocus
		txtSQL.Text = Replace(Replace(Replace(Replace(Replace(Replace(Replace(txtSQL.Text, vbTab, " "), "  ", " "), "  ", " "), "  ", " "), "  ", " "), "  ", " "), "  ", " ")
	End Sub


	Private Sub cmdConnectOleDB_Click(sender As System.Object, e As System.EventArgs) Handles cmdConnectOleDB.Click
		Dim iPos As Long, iMax As Long, iCol As Long
		Dim dtTimeStart As New Date, dtTimeEnd As New Date, lDiffTicks As Long, dtTimeDiff As Date
		Dim oconOleDB As OleDb.OleDbConnection, oCmdOleDB As OleDb.OleDbCommand
		Dim oReader As OleDb.OleDbDataReader


		Try

			cmdConnectOleDB.Enabled = False
			dtTimeStart = Now

			txtResults.Text = ""

			Application.DoEvents()
			GenConnectString()
			oconOleDB = New OleDb.OleDbConnection(msConnectStringOleDB)
			oconOleDB.Open()
			If oconOleDB.State = ConnectionState.Open Then
				If txtSQL.Text = "" Then
					txtResults.Text = "Connection OK but no SQL execution to run"
				Else
					oCmdOleDB = oconOleDB.CreateCommand
					oCmdOleDB.CommandText = txtSQL.Text
					oCmdOleDB.CommandType = CommandType.Text
					oReader = oCmdOleDB.ExecuteReader
					If oReader.HasRows Then
						txtResults.Text = ""
						iMax = 1500
						iPos = 0
						Do While iMax > iPos And oReader.Read
							Application.DoEvents()
							For iCol = 0 To oReader.FieldCount - 1
								txtResults.Text = txtResults.Text & oReader.GetValue(iCol).ToString & ", "
							Next
							txtResults.Text = txtResults.Text & vbCrLf
							iPos = iPos + 1
						Loop
						Do While oReader.Read
							iPos = iPos + 1
							Application.DoEvents()
						Loop
						dtTimeEnd = Now
						lDiffTicks = dtTimeEnd.Ticks - dtTimeStart.Ticks
						dtTimeDiff = New Date(lDiffTicks)
						txtResults.Text = "total of " & iPos & " records with Duration of " & _
						IIf(dtTimeDiff.Hour > 0, dtTimeDiff.Hour & " hour" & IIf(dtTimeDiff.Hour > 1, "s ", " "), "") & IIf(dtTimeDiff.Minute > 0, dtTimeDiff.Minute & " minute" & IIf(dtTimeDiff.Minute > 1, "s ", " "), "") & dtTimeDiff.Second & " second" & IIf(dtTimeDiff.Second > 1, "s", "") & vbCrLf & _
						txtResults.Text
						oReader.Close()
					End If
				End If
				oconOleDB.Close()

			Else
				txtResults.Text = "No connection"
			End If

		Catch ex As Exception
			txtResults.Text = txtResults.Text & vbCrLf & ex.ToString
		End Try

		cmdConnectOleDB.Enabled = True

	End Sub

	Private Sub LinkLabel1_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

		Shell("explorer.exe http://www.connectionstrings.com", AppWinStyle.NormalFocus, False)

	End Sub
End Class
